package piglatintranslator;

public class Translator {

	public static final String NIL = "nil";
	private String fullPhrase;
	private Character firstChar;
	private String translatedPhrase ="";
	private String[] multipleWords;
	private String spaceOrHypen;

	public Translator(String inputPhrase) {
		fullPhrase = inputPhrase;
	}

	public String getPhrase() {
		return fullPhrase;
	}

	public String translate() {

		if(fullPhrase.equals("")) {
			return NIL;
		} else {
			boolean res = splitWords();

			for (String phrase : multipleWords) {

				String firstLetter = phrase.substring(0,1);
				String lastLetter = phrase.substring(phrase.length()-1, phrase.length());

				boolean firstLetterIsPunct = checkCharacterPunctuation(firstLetter);
				boolean lastLetterIsPunct = checkCharacterPunctuation(lastLetter);

				if(firstLetterIsPunct) {
					phrase = phrase.substring(1, phrase.length());
				}
				if(lastLetterIsPunct) {
					phrase = phrase.substring(0, phrase.length()-1);
				}

				if(startsWithVowel(phrase)) {

					if(phrase.endsWith("y")) {
						phrase = phrase + "nay";
					} else if(endsWithVowel(phrase)) {
						phrase = phrase + "yay";
					} else if(!endsWithVowel(phrase)) {
						phrase = phrase + "ay";
					}
				} else if(!startsWithVowel(phrase) && phrase.length() != 0) {
					while(!startsWithVowel(phrase)) {
						firstChar = phrase.charAt(0);
						phrase = phrase.substring(1, phrase.length()) + firstChar;
					}
					phrase = phrase + "ay";
				}
				
				if(!res) {
					spaceOrHypen = " ";
				} else {
					spaceOrHypen = "-";
				}

				if(firstLetterIsPunct) {
					translatedPhrase = translatedPhrase.concat(firstLetter).concat(phrase).concat(spaceOrHypen);					
				} else if(lastLetterIsPunct) {
					translatedPhrase = translatedPhrase.concat(phrase).concat(lastLetter).concat(spaceOrHypen);					
				} else {
					translatedPhrase = translatedPhrase.concat(phrase).concat(spaceOrHypen);					
				}
			}
			// Returns the string after removing last character (a whitespace or hypen)
			return (translatedPhrase.substring(0, translatedPhrase.length() - 1));
		}
	}

	private boolean startsWithVowel(String phrase) {
		return(phrase.startsWith("a") || phrase.startsWith("e") || phrase.startsWith("i") || phrase.startsWith("o") || phrase.startsWith("u")); 
	}

	private boolean endsWithVowel(String phrase) {
		return(phrase.endsWith("a") || phrase.endsWith("e") || phrase.endsWith("i") || phrase.endsWith("o") || phrase.endsWith("u")); 
	}

	private boolean splitWords() {
		multipleWords = fullPhrase.split("[-\\s]");
		return (fullPhrase.contains("-"));
	}
	public boolean checkPunctuation(String word) {
		return word.matches(".*[.,:;'()!?]");
	}

	public boolean checkCharacterPunctuation(String inputPhrase) {
		return inputPhrase.equals(".") || inputPhrase.equals(",") || inputPhrase.equals(";") || inputPhrase.equals(":") || inputPhrase.equals("?") || inputPhrase.equals("!") || inputPhrase.equals("'") || inputPhrase.equals("(") || inputPhrase.equals(")");
	}
}
